---
canonical: 'https://untappd.com/user/mikestreety/checkin/1193991830'
title: Lobo
serving: Can
rating: 9
purchased: Beer No Evil
date: '2022-08-21'
breweries:
  - brewery/cloak-and-dagger/
  - brewery/hand-brew-co/
tags:
  - hazy
number: 701
permalink: beer/lobo-cloak-and-dagger-hand-brew-co-701/
---

Had this in a pub and had to find it in a can. Just a juicy full-bodied hazy. A bit too strong for a proper session but could smash a few of these in an evening. A fantastic beer
