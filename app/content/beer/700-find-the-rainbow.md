---
canonical: 'https://untappd.com/user/mikestreety/checkin/1192455594'
title: Find the Rainbow
serving: Can
rating: 9.5
purchased: Palate Bottle Shop
date: '2022-08-17'
breweries:
  - brewery/alpha-delta-brewing/
  - brewery/merakai-brewing-co/
tags:
  - neipa
number: 700
permalink: beer/find-the-rainbow-alpha-delta-brewing-merakai-brewing-co-700/
---

What an absolute banger to start in Untappd quest. Smooth and juicy and so easy to drink. Spot on at just under 6%, this NEIPA is one of the best I've had.
