---
canonical: 'https://untappd.com/user/mikestreety/checkin/1193148933'
title: Memphis Blvd
rating: 6
purchased: Aldi
date: '2022-08-19'
breweries:
  - brewery/aldi-stores-uk/
tags:
  - ipa
number: 720
permalink: beer/memphis-blvd-aldi-stores-uk-720/
---

An obvious Elvis Juice rip-off, this actually tasted better than what it was impersonating. I think this was due to the lack of grapefruit, which is quite prominent in the original. It tasted like an "ok" Aldi IPA .
