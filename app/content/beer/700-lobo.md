---
canonical: 'https://untappd.com/user/mikestreety/checkin/1193991830'
status: 200
image: >-
  https://assets.untappd.com/photos/2022_08_21/1e80ec7b6e5d1e2ba303c5ad5249312c_1280x1280.jpg
title: Lobo
serving: Can
rating: '9'
purchased_from: Beer No Evil
date: '2022-08-21'
breweries:
  - brewery/cloak-and-dagger/
  - brewery/hand-brew-co/
tags:
  - hazy
number: 700
permalink: beer/lobo-cloak-and-dagger-hand-brew-co-700/
---

Had this in a pub and had to find it in a can. Just a juicy full-bodied hazy. A bit too strong for a proper session but could smash a few of these in an evening. A fantastic beer
