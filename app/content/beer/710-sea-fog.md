---
canonical: 'https://untappd.com/user/mikestreety/checkin/1207795606'
title: Sea Fog
serving: Can
rating: 6
purchased: Adnams Cellar & Kitchen Store
date: '2022-10-02'
breweries:
  - brewery/adnams/
number: 710
permalink: beer/sea-fog-adnams-710/
---

This was a bit inconsequential as a beer. It washed over me like the weather it's named after. It was missing the fruitiness this style normally has bags of
