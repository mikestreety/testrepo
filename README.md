```js
import { Gitlab } from '@gitbeaker/node'; // All Resources

import parser from 'rss-url-parser';
import fetch from 'node-fetch';

const api = new Gitlab({
	token: '',
});

const data = await parser('https://zapier.com/engine/rss/1463736/ale-house-rock');

data.forEach(async item => {
	let fileExists = false;
	try {
		await api.RepositoryFiles.show(38315485, item.title + '.jpg', 'main');
		fileExists = true;
	} catch(e) {

	}

	if(!fileExists) {
		const imageUrlData = await fetch(item.enclosures[0].url);
		const buffer = await imageUrlData.arrayBuffer();
		const stringifiedBuffer = Buffer.from(buffer).toString('base64');

		let c = await api.Commits.create(
			38315485,
			'main',
			'Adding ' + item.title,
			[
				{
					action: 'create',
					filePath: item.title + '.jpg',
					content: stringifiedBuffer,
					encoding: "base64"
				},
			]
		);
	}
});
```


```js
import { Gitlab } from '@gitbeaker/node'; // All Resources
import matter from 'gray-matter';
import fetch from 'node-fetch';

const parse = date => new Date(Date.parse(date));

const slug = str => {
	if(str) {
		str = str.replace(/^\s+|\s+$/g, ''); // trim
		str = str.toLowerCase();

		// remove accents, swap ñ for n, etc
		var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
		var to   = "aaaaeeeeiiiioooouuuunc------";
		for (var i = 0, l = from.length ; i<l ; i++) {
			str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		}

		str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			.replace(/\s+/g, '-') // collapse whitespace and replace by -
			.replace(/-+/g, '-'); // collapse dashes
	}

	return str;
}


const api = new Gitlab({
	token: '',
});

fetch('https://alehouse.rocks/api/beers.json')
	.then(data => data.json())
	.then(async data => {
		for(let item of data) {
			let brewery = [];
			for (let index = 0; index < item.breweries.length; index++) {
				let fileExists = false;
				let path = 'app/content' + item.breweries[index].slug + '.md';
				console.log(path);

				try {
					let file = await api.RepositoryFiles.showRaw(25096202, path, {ref: 'static-data'});
					let contents = matter(file);
					if(!contents.data.beers.includes(item.slug)) {
						contents.data.beers.push(item.slug);
						contents = matter.stringify(contents.content, contents.data)
						file = await api.RepositoryFiles.edit(25096202, path, 'static-data', contents, 'Edit ' + item.breweries[index].title);
					}
					fileExists = true;
				} catch(e) {

				}

				console.log(fileExists);


				if(!fileExists) {

					brewery.title = item.breweries[index].title
					brewery.slug = item.breweries[index].slug
					brewery.beers = [item.slug]

					let data = matter.stringify("\n", brewery);

					let c = await api.Commits.create(
						25096202,
						'static-data',
						'Adding ' + brewery.title,
						[
							{
								action: 'create',
								filePath: path,
								content: data
							},
						]
					);
				}



			}


			// let c = await api.Commits.create(
			// 	25096202,
			// 	'static-data',
			// 	'Adding ' + item.title,
			// 	[
			// 		{
			// 			action: 'create',
			// 			filePath: 'app/content/beers/' + item.date + '-' + slug(item.title) + '.md',
			// 			content: data
			// 		},
			// 	]
			// );

		}
	})
```

```js
import { Gitlab } from '@gitbeaker/node'; // All Resources
import matter from 'gray-matter';
import fetch from 'node-fetch';

const parse = date => new Date(Date.parse(date));

const slug = str => {
	if(str) {
		str = str.replace(/^\s+|\s+$/g, ''); // trim
		str = str.toLowerCase();

		// remove accents, swap ñ for n, etc
		var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
		var to   = "aaaaeeeeiiiioooouuuunc------";
		for (var i = 0, l = from.length ; i<l ; i++) {
			str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		}

		str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			.replace(/\s+/g, '-') // collapse whitespace and replace by -
			.replace(/-+/g, '-'); // collapse dashes
	}

	return str;
}


const api = new Gitlab({
	token: '',
});

fetch('https://alehouse.rocks/api/beers.json')
	.then(data => data.json())
	.then(async data => {
		for(let item of data) {
			let body = item.description ;
			item.image = `/assets/images/${item.code}.webp`

			if(item.hashtags) {
				item.tags = item.hashtags.replaceAll('#', '').split(' ')
			}
			let breweries = [];

			delete item.description;
			delete item.brewery;
			delete item.code;
			delete item.hashtags;

			item.rating = parseInt(item.rating)

			for (let index = 0; index < item.breweries.length; index++) {
				breweries.push(item.breweries[index].slug);
			}
			item.breweries = breweries;

			item.date = item.date.replace('24:', '23:');
			item.date = new Date(item.date.trim()).toISOString().split('T')[0];

			let data = matter.stringify("\n" + body, item);

			console.log(item.title);

			let c = await api.Commits.create(
				25096202,
				'static-data',
				'Adding ' + item.title,
				[
					{
						action: 'create',
						filePath: 'app/content/beer/' + item.date + '-' + slug(item.title) + '.md',
						content: data
					},
				]
			);

		}
	})

// fetch('https://alehouse.rocks/api/beers.json')
// 	.then(data => data.json())
// 	.then(async data => {
// 		for(let item of data) {

// 			const imageUrlData = await fetch('https://res.cloudinary.com/mikestreety/image/upload/v2/alehouserock/' + item.image);
// 			const buffer = await imageUrlData.arrayBuffer();
// 			const stringifiedBuffer = Buffer.from(buffer).toString('base64');

// 			console.log(item.title);

// 			let c = await api.Commits.create(
// 				38315485,
// 				'main',
// 				'Adding ' + item.title,
// 				[
// 					{
// 						action: 'create',
// 						filePath: 'html/assets/images/' + item.code + '.webp',
// 						content: stringifiedBuffer,
// 						encoding: "base64"
// 					},
// 				]
// 			);
// 				console.log(c);

// 			// let c = await api.Commits.create(
// 			// 	25096202,
// 			// 	'static-data',
// 			// 	'Adding ' + item.title,
// 			// 	[
// 			// 		{
// 			// 			action: 'create',
// 			// 			filePath: 'app/content/beers/' + item.date + '-' + slug(item.title) + '.md',
// 			// 			content: data
// 			// 		},
// 			// 	]
// 			// );

// 		}
// 	})

// fetch('https://alehouse.rocks/api/beers.json')
// 	.then(data => data.json())
// 	.then(async data => {
// 		for(let item of data) {
// 			for (let index = 0; index < item.breweries.length; index++) {
// 				let data = matter.stringify("\n" + body, item);

// 			}
// 		}
// 	});
```
