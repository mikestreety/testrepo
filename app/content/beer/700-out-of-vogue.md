---
title: Out of Vogue
rating: '6.5'
date: '2022-09-20'
canonical: 'https://untappd.com/user/mikestreety/checkin/1203710389'
tags:
  - westcoastpale
breweries:
  - brewery/burning-sky-brewery/
number: 700
permalink: beer/out-of-vogue-burning-sky-brewery-700/
---

I was forced to have this after I dropped it on the garage floor. Hoppy and light, not my favourite beer but passable. Got better as I drink it
