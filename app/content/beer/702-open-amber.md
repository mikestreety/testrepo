---
canonical: 'https://untappd.com/user/mikestreety/checkin/1192751026'
title: OPEN AMBER
rating: 7.5
date: '2022-08-18'
breweries:
  - >-
    brewery/birrificio-agricolo-baladin-baladin-indipendente-italian-farm-brewery/
tags:
  - ipa
number: 702
permalink: >-
  beer/open-amber-birrificio-agricolo-baladin-baladin-indipendente-italian-farm-brewery-702/
---

A present from my sister from her recent trip to Italy. Crisp and light, with a twang - it tasted like a classic IPA. Not staggering, but tasty enough.
